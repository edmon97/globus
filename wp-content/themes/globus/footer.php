<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package globus
 */

?>

<?php wp_footer(); ?>




  <footer>
                <div class="container-fluid">
                <div class="">
                    <div class="col-md-12 footer_Page">
                        
                 
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 nopadding textlogoUnder">
                             <img class="footer__logo"src="<?php echo ot_get_option( 'logo_footer' );?>" alt=""> 
                             
                            <p> <?php echo ot_get_option( 'text_underlogo' );?></p>
                        </div>
                        <div class="col-md-2 nopadding textFooter">
                            <h3><?php echo ot_get_option( 'title_navigation' );?></h3>
                        </div>
                        <div class="col-md-4 nopadding">
                            <h3 class="titleFooterCenter"><?php echo ot_get_option( 'title__communication' );?></h3>
                            <?php echo do_shortcode('[contact-form-7 id="78" title="Contact form 1"]')?>
                        </div>
                        <div class="col-md-4 nopadding textFooter">
                            <h3><?php echo ot_get_option( 'title_contact' );?></h3>
                            <p class="textFooter1"><?php echo ot_get_option( 'address' );?></p>
                            <p class="textFooter1"><?php echo ot_get_option( 'day' );?></p>
                            <p class="textFooterNumber"><?php echo ot_get_option( 'contact_namber1' );?></p>
                            <p class="textFooterNumber"><?php echo ot_get_option( 'contact_namber2' );?></p>
                            <p class="textFooterEmail"><?php echo ot_get_option( 'email' );?></p>
                            <div class="icoSocial">
                                 <a href=""><img src="<?php echo ot_get_option( 'ico_twitter' );?>" alt=""> </a>
                                 <a href=""><img src="<?php echo ot_get_option( 'ico_facebook' );?>" alt=""> </a>
                                 <a href=""><img src="<?php echo ot_get_option( 'ico_linkedin' );?>" alt=""> </a>
                                 <a href=""><img src="<?php echo ot_get_option( 'ico_google' );?>" alt=""> </a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                   </div>
                </div>
                </footer>

</body>
</html>
