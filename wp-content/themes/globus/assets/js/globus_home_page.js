jQuery(document).ready(function(){

	/*animation for clouds*/

	var slow = 60000;
	var slowest = 80000;
	var super_slow = 130000;

	var smallest_distance = 13;
	var small_distance = 25;
	var normal_distance = 30;
	var big_distance = 40;

	var clouds = [
		{"id":1, "speed":slow,"distance":small_distance},
		{"id":2, "speed":slow,"distance":big_distance},
		{"id":3, "speed":super_slow,"distance":smallest_distance},
		{"id":4, "speed":slowest,"distance":small_distance},
		{"id":5, "speed":slowest,"distance":big_distance},
		{"id":6, "speed":super_slow,"distance":smallest_distance},
		{"id":7, "speed":super_slow,"distance":normal_distance},
		{"id":8, "speed":super_slow,"distance":smallest_distance},
		{"id":9, "speed":super_slow,"distance":normal_distance}
	];

	jQuery.each(clouds,function(k,v){
		cloud_move(v);
	});

	/*animation for clouds*/
	/*pulse btn animation*/

	jQuery(".pulse").hover(function() {
	    	var id = jQuery(this).attr("id");
		  	jQuery(".types h4:eq("+(id-1)+")").addClass("pulse_hover");
	  }, function() {
    	var id = jQuery(this).attr("id");
	  	jQuery(".types h4:eq("+(id-1)+")").removeClass("pulse_hover");
  	});

  	/*pulse btn animation*/
});

function cloud_move(cloud){
  jQuery(".cloud"+cloud["id"]).animate({
              left: '-'+cloud["distance"]+'%',
          }, 
          cloud["speed"],
          "linear",
          function(){
            jQuery(this).css({'left': '100%'});  
            cloud_move(cloud);             	
          });
  }