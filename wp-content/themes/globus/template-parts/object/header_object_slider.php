<div class="container-fluid">
        <div class="logo_slider_block">
            <div class="col-md-4 logoBlock">
                <img class="logo" src="<?php echo ot_get_option('logo_globus_white'); ?>" alt="">
                <div class="object_term">
                    <h2><?php echo the_field('title_left'); ?></h2>
                    <p id="data_left"></p>
                </div>
            </div>
            <div class="col-md-4 sliderBlock">
                <div style="height: 100%;">
                    <div class="swiper-container swiper-container-vertical">
                        <div class="swiper-wrapper">
                            <?php $args = array(
                                'post_type' => 'building',
                                'posts_per_page' => -1
                            );
                            $wp_post_new = new WP_Query($args);
                            $all_post = $wp_post_new->get_posts();

                            foreach ($all_post as $single_post_1) {
                                ?>
                                <div class="swiper-slide object-slide animated" id="<?php echo $single_post_1->ID; ?>"
                                     url="<?
                                     echo get_page_link($single_post_1->ID); ?>">
                                    <img src="<?php echo get_field("building_img_without_shadow_copy", $single_post_1->ID)["url"]; ?>"
                                         active-src="<?php echo get_field("building_img_with_shadow", $single_post_1->ID)["url"]; ?>"
                                         alt="">
                                    <h4><?php echo get_the_title($single_post_1->ID); ?></h4>
                                </div>
                                <?php
                            }

                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 dataBlock">
                <div class="object_data">
                    <h4><span id="flat_cnt"></span>&nbsp;Квартир</h4>
                    <h4><span id="commercial_cnt"></span>&nbsp;Комерції</h4>
                    <h4><span id="parking_cnt"></span>&nbsp;Парковок</h4>
                </div>
                <p id="phone"></p>
            </div>
        </div>
    </div>

