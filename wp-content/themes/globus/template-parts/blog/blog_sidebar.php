<div class="col-md-4">
<form role="search" method="get" id="searchform" class="searchform" action="
    <?php echo esc_url( home_url( '/' ) ); ?>">
        <div>
            <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
            <div class="search_form_blog">
            <input type="text" class="formSearch" placeholder="Пошук" value="<?php echo get_search_query(); ?>" name="s" id="s" />
            <span class="glyphicon glyphicon-search"></span>
            </div>
        </div>
</form>
<?php
get_sidebar();
?>
</div>

