<?php 
    /*Template Name: building page*/
    get_header(); 

?>
<?php

    $args = array (
        'post_type'=>'building',
        'posts_per_page' => -1
    );                       
    $wp_post_new = new WP_Query($args);
    $all_post = $wp_post_new->get_posts();
    
    $class_name = '';

    $building_array = array() ;

    foreach( $all_post as $single_post_1 ){
        
        $flat_count = get_field("flat_count", $single_post_1->ID );
       
        $temp_array = array(
            "flat_count" => $flat_count ,
            );
        array_push( $building_array, $temp_array ) ;
     
    } 
?>
