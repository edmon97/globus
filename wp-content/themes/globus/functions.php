<?php
/**
 * globus functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package globus
 */

if ( ! function_exists( 'globus_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function globus_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on globus, use a find and replace
	 * to change 'globus' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'globus', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'globus' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'globus_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 250,
		'width'       => 250,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'globus_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function globus_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'globus_content_width', 640 );
}
add_action( 'after_setup_theme', 'globus_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function globus_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'globus' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'globus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'globus_widgets_init' );

// add_action('wp_ajax_building','get_building');

add_action('wp_ajax_get_building', 'get_building');
add_action('wp_ajax_nopriv_get_building', 'get_building');

function get_building(){

	$postId = $_REQUEST["id"];

    $build=array();

    $build["flat_count"] = get_field("flat_count", $postId);
    $build["commercial_count"] = get_field("commercial_count",$postId);
    $build["parking_count"] = get_field("parking_count",$postId);
    $build["operation_date"] = get_field("operation_date",$postId);
    $build["phone_number"] = get_field("phone_number",$postId);
    $build["background_image"] = get_field("background_image",$postId)["url"];

    echo json_encode($build);

    wp_die();
}



/**
 * Enqueue scripts and styles.
 */
function globus_scripts() {

	// echo get_the_title();
	wp_enqueue_style( 'globus-style', get_stylesheet_uri() );

	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );

	wp_enqueue_style( 'object_page_css', get_template_directory_uri() . '/assets/css/globus_page_object.css' );

	wp_enqueue_style( 'globus_page_passport_object_css', get_template_directory_uri() . '/assets/css/globus_page_passport.css' );
	
	wp_enqueue_style( 'globus_page_blog_css', get_template_directory_uri() . '/assets/css/globus_page_blog.css' );

	wp_enqueue_style( 'globus_page_about_us_css', get_template_directory_uri() . '/assets/css/globus_page_about_us.css' );

	wp_enqueue_style( 'Header_Footer_css', get_template_directory_uri() . '/assets/css/globus_header_footer.css' );

	wp_enqueue_style( 'globus_page_contact_css', get_template_directory_uri() . '/assets/css/globus_page_contact.css' );

	wp_enqueue_style( 'swiper_carousel_css', get_template_directory_uri() . '/assets/css/swiper.min.css');
	wp_enqueue_style( 'animate_css', get_template_directory_uri() . '/assets/css/animate.css');


    wp_enqueue_script( 'SmoothScroll_js', get_template_directory_uri() . '/assets/js/SmoothScroll.min.js' );
    wp_enqueue_script( 'custom_js', get_template_directory_uri() . '/assets/js/custom.js' );
	wp_enqueue_script( 'jquery_js', get_template_directory_uri() . '/assets/js/jquery-3.2.1.min.js' );
    wp_enqueue_script( 'scroll_js', get_template_directory_uri() . '/assets/js/scroll.js' );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/assets/js/bootstrap.min.js' );

	
	wp_enqueue_script( 'swiper_js', get_template_directory_uri() . '/assets/js/swiper.min.js' );

	wp_enqueue_script( 'swiper_jquery_js', get_template_directory_uri() . '/assets/js/swiper.jquery.min.js');

	wp_enqueue_script( 'custom_comment_js', get_template_directory_uri() . '/assets/js/custom_comment.js');

	wp_enqueue_script( 'google-maps_js', get_template_directory_uri() . '/google-maps.js');

//    wp_register_script( 'googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7akG8yRXoo8Q27ugs9c2g2Hwp0m7hgJU' );
    wp_enqueue_script('googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7akG8yRXoo8Q27ugs9c2g2Hwp0m7hgJU');

	switch (get_the_title()){
		case 'Home':
		{
			wp_enqueue_style( 'home_page_css', get_template_directory_uri() . '/assets/css/globus_page_home.css');
			wp_enqueue_script( 'home-page_js', get_template_directory_uri() . '/assets/js/globus_home_page.js' );
//            wp_enqueue_style( 'slider_style_css', get_template_directory_uri() . '/assets/css/globus_slider_style.css');
            wp_enqueue_script( 'slider_js', get_template_directory_uri() . '/assets/js/globus_main_slider.js' );
		}
		break;

		case 'Objects' :
		{
			wp_enqueue_style( 'object_page_css', get_template_directory_uri() . '/assets/css/globus_page_object.css' );
			wp_enqueue_script( 'object_page_js', get_template_directory_uri() . '/assets/js/globus_object_page.js' );
            wp_enqueue_style( 'slider_style_css', get_template_directory_uri() . '/assets/css/globus_slider_style.css');
		}
		break;

		case 'Contact' :
		{
			wp_enqueue_script( 'object_page_js', get_template_directory_uri() . '/assets/js/globus_object_page.js' );
		}
		break;
	}

	// wp_enqueue_script( 'globus-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	// wp_enqueue_script( 'globus-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'globus_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**googl map**/
function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyB7akG8yRXoo8Q27ugs9c2g2Hwp0m7hgJU');
}

add_action('acf/init', 'my_acf_init');

