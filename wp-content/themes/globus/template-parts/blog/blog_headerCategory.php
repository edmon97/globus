<div class="container">
    <div class="row header__blog" >
        <?php
           $categories = get_terms('category',                                
            $args = array(
              
                    'slug' => array( 'ackcii', 
                     'novunu',
                     'hid_budivnitstva' ),
            ));
            foreach( $categories as $category ): ?>
            <div class="col-md-4">
                    <?php                      
                    $tax    = $category->taxonomy;
                    $cat_id = $tax .'_'.$category->term_id;
                     $url_id = $tax .'_'.$category->term_id;
                      ?>
                    <div class="middleBlock">
                        <a class="titleCategory" href="<?php echo get_term_link($category->term_id);?>"><?php echo $category->name; ?> </a>
                    </div>
                    <div class="news__img__theme">
                        <img class="blog__img" src="<?php echo get_field('image', $cat_id)['url']; ?>" alt="<?php echo $category>name; ?>">
                    </div>
                
            </div>
           <?php  endforeach; ?>
    </div>
</div>
