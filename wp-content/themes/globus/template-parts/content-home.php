<?php
/*Template Name: home page*/
get_header();
?>
    <div class="container-fluid">
        <div class="header-container">
            <div class="clouds">
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud3.png" class="cloud1 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud2.png" class="cloud2 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud7.png" class="cloud3 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud8.png" class="cloud4 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud2.png" class="cloud5 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud6.png" class="cloud6 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud1.png" class="cloud7 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud8.png" class="cloud8 cloud"
                     alt=""/>
                <img src="http://globus/wp-content/themes/globus/assets/images/clouds/cloud2.png" class="cloud9 cloud"
                     alt=""/>
            </div>
            <div class="container-content">
                <div class="slogan">
                    <div class="slogan_part leftTextGB">
                        <p>глобус - дім,</p>
                    </div>
                    <div class="slogan_part rightTextGB">
                        <p>де живе добро!</p>
                    </div>
                </div>
                <div class="types">
                    <h4>комерція</h4>
                    <h4>пентхаус</h4>
                    <h4>1 поверх</h4>
                    <h4>2 поверх</h4>
                </div>
            </div>
            <div class="swiper-container home-objects-slider">

                <div class="swiper-wrapper">
                    <?php $args = array(
                        'post_type' => 'building',
                        'posts_per_page' => -1
                    );
                    $wp_post_new = new WP_Query($args);
                    $all_post = $wp_post_new->get_posts();

                    foreach ($all_post as $newsPost) {
                        ?>
                        <div class="swiper-slide object-slide animated" id="<?php echo $newsPost->ID; ?>" url="<?
                        echo get_page_link($newsPost->ID); ?>">
                            <img src="<?php echo get_field("building_img_without_shadow_copy", $newsPost->ID)["url"]; ?>"
                                 active-src="<?php echo get_field("building_img_with_shadow", $newsPost->ID)["url"]; ?>"
                                 alt="">
                            <h4><?php echo get_the_title($newsPost->ID); ?></h4>
                        </div>
                        <?php
                    }

                    ?>
                </div>

                <div class="swiper-button-prev nav-control nav-control-left"></div>
                <div class="swiper-button-next nav-control nav-control-right"></div>

            </div>
            <div>
                <span class="pulse type commercial" id="1"></span>
                <span class="pulse type penthouse" id="2"></span>
                <span class="pulse type first_floor" id="3"></span>
                <span class="pulse type second_floor" id="4"></span>
            </div>
        </div>
    </div>
    <hr class="div_line">
    <div class="container-fluid"
         style="background: linear-gradient( -90deg, rgba(255,255,255, 0.40) 0%, rgba(255,255,255, 0.40) 100%), url('<?php echo get_field('background_block2')['url']; ?>') bottom/cover no-repeat;">
        <div class="container information-container">
            <div class="container-content">
                <div class="title">
                    <?php echo get_field('title_1'); ?>
                </div>
                <?php
                $rows = get_field('about_us');
                if ($rows) {
                    foreach ($rows as $row) {
                        ?>

                        <div class="post-item">
                            <div class="col-md-5 post-item-img">
                                <img src="<?php echo $row['image']['url']; ?>" alt="">
                            </div>
                            <div class="col-md-7 post-item-text">
                                <?php echo $row['text']; ?>
                                <div class="detail">
                                    <div class="link">
                                        <a href="<?php echo $row['url']; ?>">Детальніше...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }

                ?>
            </div>
        </div>
        <hr class="div_line">
        <div class="container information-container">
            <div class="container-content">

                <div class="сertificatesBlock col-md-12">
                    <div class="col-md-6 progressBlock">
                        <div class="advantageTitle">
                            <p>наші сертифікати</p>
                        </div>
                    </div>
                    <div class="brackets">
                        <img src="/wp-content/themes/globus/assets/images/right.png" alt="">
                        <img src="/wp-content/themes/globus/assets/images/left1.png" alt="">
                    </div>
                    <div class="col-md-6 progressBlock">
                        <div class="advantageTitle">
                            <p>наші партнери</p>
                        </div>
                        <div class="logoPartner">
                            <?php

                            if (function_exists('ot_get_option')) {
                                $images = explode(',', ot_get_option('partner', ''));
                                if (!empty($images)) {
                                    foreach ($images as $id) {
                                        if (!empty($id)) {
                                            $full_img_src = wp_get_attachment_image_src($id, 'custom-thumb');
                                            ?>
                                            <img src="<?php echo $full_img_src[0]; ?>" alt="">
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="div_line">
    <div class="container-fluid"
         style="background: linear-gradient( -90deg, rgba(0,0,0, 0.400) 0%, rgba(0,0,0, 0.400) 100%), url('<?php echo get_field('background_block3')['url']; ?>') center/cover no-repeat;">
        <div class="container reviews-container">
            <div class="container-content">
                <div class="title">
                    <?php echo get_field('title_2'); ?>
                </div>
                <?php $args = array(
                    'post_type' => 'reviews',

                );
                $wp_post_new = new WP_Query($args);
                $all_post = $wp_post_new->get_posts();
                foreach ($all_post as $newsPost) { ?>
                    <div class="post-item">
                        <div class="col-md-4 post-item-img">
                            <img src="<?php echo get_the_post_thumbnail_url($newsPost->ID) ?>" alt="">
                        </div>
                        <div class="col-md-7 post-item-text">
                            <p>
                                <?php echo($newsPost->post_content); ?>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <hr class="div_line">
    <div class="container-fluid"
         style="background: linear-gradient( -90deg, rgba(255,255,255, 0.40) 0%, rgba(255,255,255, 0.40) 100%), url('<?php echo get_field('background_block4')['url']; ?>') bottom/cover no-repeat;">
        <div class="container news-container">
            <div class="container-content">
                <div class="title">
                    <?php echo get_field('title_3'); ?>
                </div>
            </div>

            <div class="swiper-container news-slider">

                <div class="swiper-wrapper news-slider-wrapper">
                    <?php
                    $all_post = get_posts(array('numberposts' => -1, 'category_name' => 'hid_budivnitstva'));
                    foreach ($all_post as $newsPost) { ?>
                        <div class=" swiper-slide news-slide">
                            <div class="slide-content">
                                <div class="slide-image"
                                     style="background: url(<?php echo get_the_post_thumbnail_url($newsPost->ID); ?>) bottom left/cover no-repeat; ">
                                </div>
                                <div class="slide-text">
                                    <h3><?php echo get_the_title($newsPost->ID); ?></h3>
                                    <p>
                                        <?php echo($newsPost->post_excerpt); ?>
                                    </p>
                                </div>
                                <div class="slide-detail">
                                    <a class="" href="<?php echo get_permalink($newsPost->ID); ?>">Детальніше</a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                </div>
                <div class="swiper-button-prev news-prev-btn nav-control nav-control-left"></div>
                <div class="swiper-button-next news-next-btn nav-control nav-control-right"></div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>