<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package globus
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="wp_ajax" content="<?php echo get_site_url().'/wp-admin/admin-ajax.php'; ?>" >
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<div class="header">
            
    <div class="menuHeader">
  <?php 
                    wp_nav_menu( array(
                            'menu_class'=>'menuHeader',
                            'theme_location'=>'headerRight',
         
            ) );
   ?>

       </div>
</div>
