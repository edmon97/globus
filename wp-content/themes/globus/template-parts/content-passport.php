<?php
/*Template Name: passport_object page*/
get_header();
?>
<?php
    wp_enqueue_script( 'passport_page_js', get_template_directory_uri() . '/assets/js/globus_passport_page.js' );
?>
    <div class="container-fluid passport_object nopadding"
         style="background:url('<?php echo get_field('background_block1')['url']; ?>');     background-size: cover;">
        <div class="row title-row">
            <div class="col-md-3">
                <a href="">
                    <img class="passport_logo" src="<?php echo ot_get_option('logo_globus_white'); ?>" alt="">
                </a>
            </div>
            <div class="col-md-4 object_title">
                <h4>
                    <?php echo get_field('title1'); ?>
                </h4>
            </div>
        </div>
        <div class="row choose-row nopadding">
            <div class="col-md-2 PasssportRight nopadding">
                <h4>Вибір квартири</h4>
                <h4>Вибір Комори</h4>
                <h4>Вибір комерції</h4>
                <h4>Вибір Паркінгу</h4>
                <?php //echo get_field('phone_right'); ?>
            </div>
        </div>
        <div class="row infrastructure-row">
            <div class="col-md-4 PasssportLeft">
                <ul>
                    <li>Тихий спальний район</li>
                    <li>Школа</li>
                    <li>Дитячий садок</li>
                    <li>Аптека</li>
                    <li>Супермаркет</li>
                    <li>Торговий центр</li>
                    <img src="http://globus/wp-content/themes/globus/assets/images/Efect.png" class="Polosu" alt=""/>
                </ul>
            </div>
        </div>
        <div class="passport-slider swiper-container">

            <div class="swiper-wrapper">
                <?php $args = array(
                    'post_type' => 'building',
                    'posts_per_page' => -1
                );
                $wp_post_new = new WP_Query($args);
                $all_post = $wp_post_new->get_posts();

                foreach ($all_post as $single_post_1) {
                    ?>
                    <div class="swiper-slide object-slide animated" id="<?php echo $single_post_1->ID; ?>" url="<?
                    echo get_page_link($single_post_1->ID); ?>">
                        <img src="<?php echo get_field("building_img_without_shadow_copy", $single_post_1->ID)["url"]; ?>"
                             active-src="<?php echo get_field("building_img_with_shadow", $single_post_1->ID)["url"]; ?>"
                             alt="">
                        <h4><?php echo get_the_title($single_post_1->ID); ?></h4>
                    </div>
                    <?php
                }

                ?>
            </div>

            <div class="swiper-button-prev nav-control nav-control-left"></div>
            <div class="swiper-button-next nav-control nav-control-right"></div>

        </div>
    </div>
    <hr class="div_line">
    <div class="container-fluid"
         style="background-image: linear-gradient( rgba(0, 0, 0, 0.502) 0%, rgba(0, 0, 0, 0.502) 100%), url('<?php echo get_field('background_block2')['url']; ?>');background-size: cover;">
        <h2 class="container-caption caption">Як придбати квартиру?</h2>
        <div class="">


            <div id="block-container">
                <div class="svg-wrapper">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 42"
                         class="main_svg" version="1.1" id="tc-svg-2">
                        <path d="M5 0 H99.16 V25 L95.83 41.25 H0.83 V16.66 L5 0 Z" id="shape-1"
                              class="shape inactive"/>
                        <foreignobject class="node" x="0" y="0" >
<!--                            <div class="svg-div">-->
                                <span class="svg-div-title">ВІДДІЛ ПРОДАЖУ</span>
                                <p class="svg-div-text">вул. НАУКОВА 3</p>
<!--                            </div>-->
                        </foreignobject>
                        </path>
                    </svg>
                </div>
                <div class="arrow"></div>
                <div class="svg-wrapper">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 42"
                         class="main_svg" version="1.1" id="tc-svg-2">
                        <path d="M5 0 H99.16 V25 L95.83 41.25 H0.83 V16.66 L5 0 Z" id="shape-2"
                              class="shape inactive"/>
                        <foreignobject class="node" x="0" y="0">
                            <div class="svg-div">
                                <span class="svg-div-title">ПОКАЗ ОБ'ЄКТА</span>
                                <p class="svg-div-text">ВИЇЗД НА ДІЛЯНКУ З КЛІЄНТОМ</p>
                            </div>
                        </foreignobject>
                        </path>
                    </svg>
                </div>
                <div class="arrow"></div>
                <div class="svg-wrapper">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 42"
                         class="main_svg" version="1.1" id="tc-svg-2">
                        <path d="M5 0 H99.16 V25 L95.83 41.25 H0.83 V16.66 L5 0 Z" id="shape-3"
                              class="shape inactive"/>
                        <foreignobject class="node" x="0" y="0">
                            <div class="svg-div">
                                <span class="svg-div-title">УГОДА</span>
                                <p class="svg-div-text">ОФОРМЛЕННЯ ЗА 30 ХВ </p>
                            </div>
                        </foreignobject>
                        </path>
                    </svg>
                </div>
                <div class="arrow"></div>
                <div class="svg-wrapper">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 42"
                         class="main_svg" version="1.1" id="tc-svg-2">
                        <path d="M5 0 H99.16 V25 L95.83 41.25 H0.83 V16.66 L5 0 Z" id="shape-4"
                              class="shape inactive"/>
                        <foreignobject class="node" x="0" y="0" >
                            <div class="svg-div">
                                <span class="svg-div-title">ОПЛАТА</span>
                                <p class="svg-div-text">БЕЗГОТІВКОВИЙ РОЗРАХУНОК</p>
                            </div>
                        </foreignobject>
                        </path>
                    </svg>
                </div>
            </div>
        </div>
    </div>
    </div>
    <hr class="div_line">
    <div class="container-fluid"
         style="  background-image: url('<?php echo get_field('background_block3')['url']; ?>');     background-size: cover;">
        <h2 class="container-caption caption">Детальніше про об'єкт</h2>
        <div class="">
            <div class="col-md-4 moreObject nopadding">
                <h2 class="section_caption">Житловий комплекс бiзнес-класу</h2>
                <ul class="apartment_moreObjectLeft">
                    <li>Панорамні та світлі вікна з подвійним енергозберігаючим склопакетом</li>
                    <li>Металеві броньовані протипожежні двері</li>
                    <li>Індивідуальне опалення – двохфункційний котел з під’єднаними радіаторами</li>
                    <li>Розвинута інфраструктура</li>
                    <li>Спортивні та дитячі майданчики</li>
                    <li>Індивідуальні лічильники світла, газу та води</li>
                    <li>Великі холи</li>
                    <li>Інтернет та телебачення</li>
                </ul>
            </div>
        </div>
    </div>
    <hr class="div_line">
    <div class="container-fluid">
        <div class="">
            <div class="col-md-12 apartment_choice nopadding"
                 style="  background-image: linear-gradient( rgba(255, 255, 255, 0.702) 0%, rgba(255, 255, 255, 0.702) 100%), url('<?php echo get_field('background_block4')['url']; ?>'); background-size: cover;">
                <h2 class="caption">Вибір квартири</h2>
            </div>
        </div>
    </div>
    <hr class="div_line">
<?php
get_footer();
?>