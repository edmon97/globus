<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package globus
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}
?>

<div id="comments" class="comments-area">

    <?php
    // You can start editing here -- including this comment!
    if ( have_comments() ) : ?>



                <h2 class="comments-title">


                    <?php
                    printf( // WPCS: XSS OK.
                        esc_html( _nx( 'Коментарії', 'Коментарії : %1$s
                    ', get_comments_number(), 'comments title', 'globus' ) ),
                        number_format_i18n( get_comments_number() ),
                        '<span>' . get_the_title() . '</span>'
                    );
                    ?>
                </h2><!-- .comments-title -->


    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
        <nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
            <h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'globus' ); ?></h2>
            <div class="nav-links">

                <div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'globus' ) ); ?></div>
                <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'globus' ) ); ?></div>

            </div><!-- .nav-links -->
        </nav><!-- #comment-nav-above -->
    <?php endif; // Check for comment navigation. ?>



    <?php
    function mytheme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div id="comment-<?php comment_ID(); ?>">
            <div class="comment-author vcard">
                <?php echo get_avatar( 'email@example.com', 50 ); ?>


                <?php printf(__('<cite class="fn">%s</cite> <span class=""> : </span>'), get_comment_author_link()) ?><?php comment_text() ?>
            </div>
            <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br />
            <?php endif; ?>

            <!--
      <div class="comment-meta commentmetadata">
          <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
              <?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?>
          </a>
          <?php edit_comment_link(__('(Edit)'),'  ','') ?>
      </div>
-->



            <!--
      <div class="reply">
         <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>
     </div>
-->
            <?php
            }
            ?>









            <ol class="comment-list">
                <?php
                wp_list_comments( array(
//					'style'      => 'ol',
//					'short_ping' => true,
                    'callback'=>'mytheme_comment'
                ) );
                ?>
            </ol><!-- .comment-list -->

            <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
                <nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
                    <h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'globus' ); ?></h2>
                    <div class="nav-links">

                        <div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'globus' ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'globus' ) ); ?></div>

                    </div><!-- .nav-links -->
                </nav><!-- #comment-nav-below -->
                <?php
            endif; // Check for comment navigation.

            endif; // Check for have_comments().


            // If comments are closed and there are comments, let's leave a little note, shall we?
            if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

                <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'globus' ); ?></p>
                <?php
            endif;

            comment_form();
            ?>

        </div><!-- #comments -->



