$(document).ready(function () {

    setImagesAttr("data-src", "src");

    var swiper = new Swiper('.passport-slider', {
        slidesPerView: 3,
        loop: true,
        mousewheelControl: true,
        spaceBetween: 1,
        speed: 1700,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',

        // jQuery(".swiper-slide-active").html()
        // jQuery(".swiper-slide-next").html()
        // jQuery(".swiper-slide-next").nextAll(':eq(0)').html()
        onSlideChangeStart:function(){
            setImagesAttr("src", "data-src");
            jQuery(".swiper-slide-next img").attr("src", jQuery(".swiper-slide-next img").attr("active-src"));
        },
        onSlideChangeEnd: function (swiper) {
            jQuery(".swiper-slide.fadeIn, .swiper-slide.fadeOut").removeClass("fadeIn fadeOut");
        },
        //down right
        onSlideNextStart: function (swiper) {
            jQuery(".swiper-slide-prev").addClass("fadeOut");
            jQuery(".swiper-slide-next").nextAll(':eq(0)').addClass("fadeIn");
        },
        //up left
        onSlidePrevStart: function (swiper) {
            jQuery(".swiper-slide-active").addClass("fadeIn");
            jQuery(".swiper-slide-next").nextAll(':eq(1)').addClass("fadeOut");
        },
    });

    $(window).scroll(function(){
        checkAnimation();
    });

});

function setImagesAttr(name, value) {
    jQuery(".swiper-slide").each(function() {
        jQuery("img", this).attr(name, jQuery("img", this).attr(value));
        jQuery(this).removeClass("big");
        jQuery(this).removeClass("normal");
        jQuery(".swiper-slide.animated").removeClass("fadeOut fadeIn");
    });
}

function animateSvg(shapeid) {
    jQuery("#shape-" + shapeid).removeClass("inactive");
    if (shapeid < 4)
        setTimeout(function () {
            animateSvg(shapeid + 1);
        }, 1500);
}

function isElementInViewport(elem) {
    var $elem = $(elem);

    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    var elemTop = Math.round( $elem.offset().top );
    var elemBottom = elemTop + $elem.height();

    return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
}

function checkAnimation() {
    var $elem = $('#block-container');

    if ($elem.hasClass('start')) return;

    if (isElementInViewport($elem)) {
        animateSvg(1);
    }
}