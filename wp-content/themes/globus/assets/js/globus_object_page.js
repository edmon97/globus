jQuery(document).ready(function() {

    setImagesAttr("data-src", "src");

    var speedTime = 1500;

    var swiper = new Swiper('.swiper-container', {
        loop: true,
        direction: 'vertical',
        mousewheelControl: true,
        slidesPerView: 4,
        speed: speedTime,

        onSlideChangeStart: function() {
            setImagesAttr("src", "data-src");
            jQuery(".swiper-slide-next img").attr("src", jQuery(".swiper-slide-next img").attr("active-src"));

            var postId = jQuery(".swiper-slide-next").attr("id");
            var url = window.location.origin+"/wp-admin/admin-ajax.php?action=get_building";

           jQuery.post(url,{"id": postId}, function(data) {
              data= JSON.parse(data); 
              jQuery("#flat_cnt").text(data["flat_count"]);
              jQuery("#commercial_cnt").text(data["commercial_count"]);
              jQuery("#parking_cnt").text(data["parking_count"]);
              var phone_str = "<span style='color: #27d257;'>"+data["phone_number"].slice(0,3)+"</span>"+data["phone_number"].slice(3);
              // var phone_str = data["phone_number"];
              jQuery("#phone").html(phone_str);
              jQuery("#data_left").text(data["operation_date"]);
            });
            
            jQuery(".swiper-slide-next").removeClass("normal").addClass("scale big");
            jQuery(".swiper-slide-active, .swiper-slide-prev").removeClass("big").addClass("scale normal");
        },
        onSlideNextStart: function(s) {
            jQuery(".swiper-slide-prev").addClass("fadeOut");
            jQuery(".swiper-slide-next").nextAll(':eq(1)').css("opacity", "0").animate({ opacity: 1 }, speedTime);
        },
        onSlidePrevStart: function(s) {
            jQuery(".swiper-slide-active").addClass("fadeIn");
            jQuery(".swiper-slide-next").nextAll(':eq(2)').css("opacity", "1").animate({ opacity: 0 }, speedTime / 2,function(){jQuery(this).css("opacity", "1");});
        }
    });

    jQuery(".swiper-slide").click(slideClick);
});

function setImagesAttr(name, value) {
    jQuery(".swiper-slide").each(function() {
        jQuery("img", this).attr(name, jQuery("img", this).attr(value));
        jQuery(this).removeClass("big");
        jQuery(this).removeClass("normal");
        jQuery(".swiper-slide.animated").removeClass("fadeOut fadeIn");
    });
}

function slideClick(){
    // window.location.href = jQuery(this).attr("url");
    window.open(jQuery(this).attr("url"), '_blank');
}