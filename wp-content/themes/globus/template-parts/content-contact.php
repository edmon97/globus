<?php 
	/*Template Name: contact page*/
	get_header(); 
?>
<div style="background:url('<?php echo get_field('background_block1')['url']; ?>');" class="main_wrapper">
<?php
include ('/object/header_object_slider.php');
?>
</div>
	<hr class="div_line">
<div class="container-fluid">
    <?php if( have_rows('locations') ): ?>
		<div class="acf-map">
            <?php while ( have_rows('locations') ) : the_row();

                $location = get_sub_field('location');

                ?>
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
					<h4><?php the_sub_field('title'); ?></h4>
					<p class="address"><?php echo $location['address']; ?></p>
					<p><?php the_sub_field('description'); ?></p>
				</div>
            <?php endwhile; ?>
		</div>
    <?php endif; ?>
</div>
<?php
get_footer();
?>
