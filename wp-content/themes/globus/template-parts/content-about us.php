<?php
/*Template Name: about us page*/
get_header();
?>

    <div class="container-fluid about_us"
         style="background:url('<?php echo get_field('background_block1')['url']; ?>'); height: 100vh; background-size: 100% 100%;">
        <div class="col-md-4 about_us__blockLeft">
            <img class="logo" src="<?php echo ot_get_option('logo_globus_white'); ?>" alt="">

        </div>
        <div class="col-md-8 about_us__blockRight">
            <div class="about_us_vraper_text">
                <h3><?php echo get_field('title1') ?></h3>
                <p> <?php echo get_field('text__after_title1') ?></p>
            </div>
        </div>
    </div>
    <hr class="div_line">
    <div class="container"
         style="background:url('<?php echo get_field('background_block2')['url']; ?>'); background-size: 100% 100%;">
        <h3 class="title_about_us"> <?php echo get_field('title2') ?></h3>
    </div>
<?php
get_footer();
