$(document).ready(function () {

    var swiper1 = new Swiper('.news-slider', {
        slidesPerView: 3,
        loop: true,
        mousewheelControl: true,
        speed: 1000,
        roundLengths:true,
        nextButton: '.news-next-btn',
        prevButton: '.news-prev-btn',
        onSlideChangeStart:function(){
            console.log("slideChangeStart");

            jQuery(".swiper-slide-after-next").removeClass("swiper-slide-after-next");
            jQuery(".news-slide.swiper-slide-next").nextAll(":eq(0)").addClass("swiper-slide-after-next");

            // jQuery(".news-slide .slider-block.large").removeClass("large");
            // var nextSlideImg = jQuery(".swiper-slide-next.news-slide .slider-block");
            // nextSlideImg.addClass('large');
        }
    });

    setImagesAttr("data-src", "src");

    var swiper = new Swiper('.home-objects-slider', {
        slidesPerView: 3,
        loop: true,
        mousewheelControl: true,
        spaceBetween: 1,
        speed: 1700,
        nextButton: '.object-slider-button-next',
        prevButton: '.object-slider-button-prev',

        // jQuery(".swiper-slide-active").html()
        // jQuery(".swiper-slide-next").html()
        // jQuery(".swiper-slide-next").nextAll(':eq(0)').html()
        onSlideChangeStart:function(){

            var postId = jQuery(".object-slide.swiper-slide-next").attr("id");
            var url = jQuery('meta[name=wp_ajax]').attr("content");

            jQuery.post(url,{"action":"get_building","id": postId}, function(data) {
                data= JSON.parse(data);
                jQuery(".header-container").attr("style","background:url('"+data["background_image"]+"') center/cover no-repeat;");
            });

            setImagesAttr("src", "data-src");
            var nextSlideImg = jQuery(".object-slide.swiper-slide-next img");
            nextSlideImg.attr("src", nextSlideImg.attr("active-src"));
        },
        onSlideChangeEnd: function (swiper) {
            jQuery(".object-slide.swiper-slide.fadeIn, .object-slide.swiper-slide.fadeOut").removeClass("fadeIn fadeOut");
        },
        //down right
        onSlideNextStart: function (swiper) {
            jQuery(".object-slide.swiper-slide-prev").addClass("fadeOut");
            jQuery(".object-slide.swiper-slide-next").nextAll(':eq(0)').addClass("fadeIn");
        },
        //up left
        onSlidePrevStart: function (swiper) {
            jQuery(".object-slide.swiper-slide-active").addClass("fadeIn");
            jQuery(".object-slide.swiper-slide-next").nextAll(':eq(1)').addClass("fadeOut");
        },
    });


});

function setImagesAttr(name, value) {
    jQuery(".object-slide.swiper-slide").each(function() {
        jQuery("img", this).attr(name, jQuery("img", this).attr(value));
        jQuery(this).removeClass("big");
        jQuery(this).removeClass("normal");
        jQuery(".object-slide.swiper-slide.animated").removeClass("fadeOut fadeIn");
    });
}