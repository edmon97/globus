<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package globus
 */


get_header(); ?>
<div class="container-fluid">

    <div class="col-md-12 blog" style="background-image: linear-gradient( rgba(255, 255, 255, 0.702) 0%, rgba(255, 255, 255, 0.702) 100%), url('<?php $back=get_field('background_block', 38); echo $back [url];?>'); background-size: cover; padding: 132px 0 0 0;">
        <img class="blog_logo" src="<?php echo ot_get_option( 'logo_globus_blue' );?>" alt="">
        <h2 class="titleBlog">blog</h2>

        <?php
        include ('/template-parts/blog/blog_headerCategory.php');
        ?>

        <hr class="div_line">

        <div class="container">
            <div class="row blog__main">
                <?php
                include ('/template-parts/blog/blog_block_Post_single.php');
                ?>
                <?php
                include ('/template-parts/blog/blog_sidebar.php');
                ?>



            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>



<?php
///**
// * The template for displaying all single posts
// *
// * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
// *
// * @package globus
// */
//
//get_header(); ?>
<!---->
<!--    	<div id="primary" class="content-area">-->
<!--    		<main id="main" class="site-main">-->
<!--    -->
<!--    		--><?php
//		while ( have_posts() ) : the_post();
//
//			get_template_part( 'template-parts/content', get_post_format() );
//
//			the_post_navigation();
//
//			// If comments are open or we have at least one comment, load up the comment template.
//			if ( comments_open() || get_comments_number() ) :
//				comments_template();
//			endif;
//
//		endwhile; // End of the loop.
//		?>
<!--    		</main>-->
<!-- #main -->
<!--    	</div>-->
<!-- #primary -->
<!---->
<?php
//get_sidebar();
//get_footer();
//?>