<?php
/*Template Name: object page*/
get_header();
?>
<div style="background:url('<?php echo get_field('background_block1')['url']; ?>');" class="main_wrapper">
<?php
include ('/object/header_object_slider.php');
?>
</div>
<div class="container-fluid">

    <div class="headerReliably">
        <h1 class="ReliablyTitle"><?php echo the_field('title_1'); ?></h1>
    </div>

</div>
<div class="container-fluid"
     style="background:linear-gradient(rgba(73, 72, 70, 0.502) 0%, rgba(174, 119, 59, 0.502) 100%),url('<?php echo get_field('background_block2')['url']; ?>'); background-size: cover;">
    <div class="container advantagesContainer">
        <?php if (get_field('reliability')): ?>
            <?php while (has_sub_field('reliability')): ?>
                <div class="col-md-4 advantagesBlock  ">
                    <img src="<?php echo get_sub_field('image')['url']; ?>"
                         alt="<?php echo get_sub_field('image')['alt']; ?>">
                    <div class="advantageTitle"><?php echo get_sub_field('title'); ?></div>
                    <div class="advantagesDescription">
                        <p><?php echo get_sub_field('text'); ?></p>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
    <hr class="div_line">
    <div class="container-fluid">
        <div class="container сertificatesBlock">
            <div class="col-md-6 progressBlock">
                <div class="advantageTitle">
                    <p>наші сертифікати</p>
                </div>
            </div>
            <div class="brackets">
                <img src="/wp-content/themes/globus/assets/images/right.png" alt="">
                <img src="/wp-content/themes/globus/assets/images/left1.png" alt="">
            </div>
            <div class="col-md-6 progressBlock">
                <div class="advantageTitle">
                    <p>наші партнери</p>
                </div>
                <div class="logoPartner">
                    <?php

                    if ( function_exists( 'ot_get_option' ) ) {
                        $images = explode( ',', ot_get_option( 'partner', '' ) );
                        if ( ! empty( $images ) ) {
                            foreach( $images as $id ) {
                                if ( ! empty( $id ) ) {
                                    $full_img_src = wp_get_attachment_image_src( $id, 'custom-thumb' );
                                    ?>
                                    <img src="<?php echo $full_img_src[0]; ?>" alt="">
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr class="div_line">
    <footer>
        <div class="container-fluid">
            <div class="col-md-12 footer_object">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2  textlogoUnder">
                            <img class="footer__logo" src="<?php echo ot_get_option('logo_footer'); ?>"
                                 alt="">
                            <p> <?php echo ot_get_option('text_underlogo'); ?></p>
                        </div>
                        <div class="col-md-2  textFooter">
                            <h3><?php echo ot_get_option('title_navigation'); ?></h3>
                        </div>
                        <div class="col-md-4 ">
                            <h3 class="titleFooterCenter"><?php echo ot_get_option('title__communication'); ?></h3>
                            <?php echo do_shortcode('[contact-form-7 id="78" title="Contact form 1"]') ?>
                        </div>
                        <div class="col-md-4  textFooter">
                            <h3><?php echo ot_get_option('title_contact'); ?></h3>
                            <p class="textFooter1"><?php echo ot_get_option('address'); ?></p>
                            <p class="textFooter1"><?php echo ot_get_option('day'); ?></p>
                            <p class="textFooterNumber"><?php echo ot_get_option('contact_namber1'); ?></p>
                            <p class="textFooterNumber"><?php echo ot_get_option('contact_namber2'); ?></p>
                            <p class="textFooterEmail"><?php echo ot_get_option('email'); ?></p>
                            <div class="icoSocial">
                                <a href=""><img src="<?php echo ot_get_option('ico_twitter'); ?>" alt="">
                                </a>
                                <a href=""><img src="<?php echo ot_get_option('ico_facebook'); ?>" alt="">
                                </a>
                                <a href=""><img src="<?php echo ot_get_option('ico_linkedin'); ?>" alt="">
                                </a>
                                <a href=""><img src="<?php echo ot_get_option('ico_google'); ?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>