
<div class="col-md-8 blog__blockLeft">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <?php
            if ( have_posts() ) :

                if ( is_home() && ! is_front_page() ) : ?>
                    <?php
                endif;

                /* Start the Loop */
                while ( have_posts() ) : the_post();?>
                    <div class="blog__blockLeft__title" >
                        <a  href="<?php echo get_post_permalink()?>"> <?php echo get_the_title();?> </a>
                    </div>
                    <p class="blog__blockLeft__date"><?php echo get_the_time('j.m.Y');?> </p>
                    <div class="imgPost">
                        <div class="textDefaultLike">
                            <?php if( function_exists('dot_irecommendthis') ) dot_irecommendthis($single_post_1->ID); ?>
                        </div>
                        <?php the_post_thumbnail();?>


                    </div>
                    <div class="blog__blockLeft__excerpt"><?php the_excerpt(); ?>

                    </div>
                    <div class="wraper_down_post">
                        <div class="container-fluid nopadding">

                            <div class="col-md-7 right_share">
                                <?php echo do_shortcode('[ssba]'); ?>
                            </div>

                        </div>


                        <hr class="div_line_post">
                    </div>
                <?php endwhile;?>
                <?php
                $args = array(
                    'prev_text' => __('<span class="glyphicon glyphicon-chevron-left"></span> Назад'),
                    'next_text' => __('Далі  <span class="glyphicon glyphicon-chevron-right"></span>'),
                );?>
                <div class="paginate_links_blog">
                    <?php  echo paginate_links( $args );?>
                </div>

                <?php
            else :

                get_template_part( 'template-parts/content', 'none' );

            endif; ?>
            <?php
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
            ?>
        </main><!-- #main -->
    </div><!-- #primary -->
</div>









